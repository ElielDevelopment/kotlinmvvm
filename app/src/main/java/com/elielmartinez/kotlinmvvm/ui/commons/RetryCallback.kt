package com.elielmartinez.kotlinmvvm.ui.commons

/**
 * Generic interface for retry buttons.
 */
interface RetryCallback {
    fun retry()
}