package com.elielmartinez.kotlinmvvm.api

import com.elielmartinez.kotlinmvvm.model.Post
import io.reactivex.Observable
import retrofit2.http.GET

interface PostApi {
    /**
     * Get The list of posts from the API
     */
    @GET("/posts")
    fun getPosts(): Observable<List<Post>>
}