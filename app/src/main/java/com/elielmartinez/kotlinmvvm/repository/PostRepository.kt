package com.elielmartinez.kotlinmvvm.repository

import androidx.lifecycle.LiveData
import com.elielmartinez.kotlinmvvm.AppExecutors
import com.elielmartinez.kotlinmvvm.api.PostApi
import com.elielmartinez.kotlinmvvm.api.Resource
import com.elielmartinez.kotlinmvvm.database.AppDatabase
import com.elielmartinez.kotlinmvvm.database.dao.PostDao
import com.elielmartinez.kotlinmvvm.model.Post
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PostRepository @Inject constructor(
    private val appExecutors: AppExecutors,
    private val postDao: PostDao,
    private val service: PostApi
) {
    fun loadPosts(): LiveData<Resource<List<Post>>> {
        return object : RepositoryResource<List<Post>>(appExecutors) {
            override fun saveCallResult(item: List<Post>) {
                postDao.insertAll(*item.toTypedArray())
            }

            override fun shouldFetch(data: List<Post>?): Boolean {
                return data == null || data.isEmpty()
            }

            override fun loadFromDb(): LiveData<List<Post>> = postDao.all

            override fun createCall(): Observable<List<Post>> = service.getPosts()

        }.asLiveData()
     }
}