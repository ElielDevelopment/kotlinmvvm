package com.elielmartinez.kotlinmvvm.repository

import androidx.annotation.MainThread
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.LiveDataReactiveStreams
import androidx.lifecycle.MediatorLiveData
import com.elielmartinez.kotlinmvvm.AppExecutors
import com.elielmartinez.kotlinmvvm.api.Resource
import com.elielmartinez.kotlinmvvm.api.Status
import io.reactivex.BackpressureStrategy
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers

abstract class RepositoryResource<ResultType>
@MainThread constructor(private val appExecutors: AppExecutors) {
    private val result = MediatorLiveData<Resource<ResultType>>()

    init {
        result.value = Resource.loading()
        val dbSource = loadFromDb()
        result.addSource(dbSource) { data ->
            result.removeSource(dbSource)
            if (shouldFetch(data)) {
                fetchFromNetwork(dbSource)
            } else {
                result.addSource(dbSource) { newData ->
                    setValue(Resource.success(newData))
                }
            }
        }
    }

    @MainThread
    private fun setValue(newValue: Resource<ResultType>) {
        if (result.value != newValue) {
            result.value = newValue
        }
    }

    private fun fetchFromNetwork(dbSource: LiveData<ResultType>) {
        val apiResponse = createCall()

        // we re-attach dbSource as a new source, it will dispatch its latest value quickly
        result.addSource(dbSource) {
            setValue(Resource.loading())
        }
        val apiSource = transformLiveData(apiResponse)

        result.addSource(apiSource) { response ->
            result.removeSource(apiSource)
            result.removeSource(dbSource)
            when (response.status) {
                Status.SUCCESS -> {
                    appExecutors.diskIO().execute {
                        response.data?.let { saveCallResult(it) }
                        appExecutors.mainThread().execute {
                            result.addSource(loadFromDb()) { newDbData ->
                                setValue(Resource.success(newDbData))
                            }
                        }
                    }
                }
                Status.ERROR -> {
                    onFetchedFailded()
                    result.addSource(dbSource) {
                        setValue(Resource.error(response.error))
                    }
                }
                Status.LOADING -> {
                    setValue(Resource.loading())
                }
            }
        }

    }

    protected open fun onFetchedFailded() {}

    fun asLiveData() = result as LiveData<Resource<ResultType>>

    @WorkerThread
    protected abstract fun saveCallResult(item: ResultType)

    @MainThread
    protected abstract fun shouldFetch(data: ResultType?): Boolean

    @MainThread
    protected abstract fun loadFromDb(): LiveData<ResultType>

    @MainThread
    protected abstract fun createCall(): Observable<ResultType>

    private fun transformLiveData(input: Observable<ResultType>): LiveData<Resource<ResultType>> {
        val flowable = input.subscribeOn(Schedulers.io())
            .map { resource ->  Resource.success(resource) }
            .onErrorReturn { throwable -> Resource.error(throwable) }
            .toFlowable(BackpressureStrategy.MISSING)
        return LiveDataReactiveStreams.fromPublisher(flowable)
    }


}
