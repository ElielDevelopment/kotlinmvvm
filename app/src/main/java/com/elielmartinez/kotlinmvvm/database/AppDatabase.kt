package com.elielmartinez.kotlinmvvm.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.elielmartinez.kotlinmvvm.database.dao.PostDao
import com.elielmartinez.kotlinmvvm.model.Post

@Database(entities = [Post::class], version = 1)
abstract class AppDatabase: RoomDatabase() {
    abstract fun postDao(): PostDao
}