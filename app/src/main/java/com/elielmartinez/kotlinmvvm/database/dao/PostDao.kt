package com.elielmartinez.kotlinmvvm.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.elielmartinez.kotlinmvvm.model.Post

@Dao
interface PostDao {
    @get:Query("SELECT * FROM post")
    val all: LiveData<List<Post>>

    @Insert
    fun insertAll(vararg posts: Post)
}
