package com.elielmartinez.kotlinmvvm.features.postlist

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.elielmartinez.kotlinmvvm.api.Resource
import com.elielmartinez.kotlinmvvm.model.Post
import com.elielmartinez.kotlinmvvm.repository.PostRepository
import javax.inject.Inject

class PostListViewModel
@Inject constructor(private val postRepository: PostRepository) : ViewModel() {

    lateinit var postList: LiveData<Resource<List<Post>>>

    init {
        loadPosts()
    }

    private fun loadPosts() {
        postList = postRepository.loadPosts()
    }

    fun retry() {
        loadPosts()
    }
}