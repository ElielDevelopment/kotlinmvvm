package com.elielmartinez.kotlinmvvm.features.postlist

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.elielmartinez.kotlinmvvm.R
import com.elielmartinez.kotlinmvvm.databinding.ActivityMainBinding
import com.elielmartinez.kotlinmvvm.ui.commons.RetryCallback
import dagger.android.AndroidInjection
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private lateinit var binding: ActivityMainBinding

    private val postListAdapter: PostListAdapter = PostListAdapter()

    val postListViewModel: PostListViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(PostListViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.retryCallback = object : RetryCallback {
            override fun retry() {
                postListViewModel.retry()
            }
        }

        binding.postList.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        binding.postList.adapter = postListAdapter

        binding.viewModel = postListViewModel
    }

}
