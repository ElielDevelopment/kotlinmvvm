package com.elielmartinez.kotlinmvvm.util

import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.elielmartinez.kotlinmvvm.api.Resource
import com.elielmartinez.kotlinmvvm.api.Status
import com.elielmartinez.kotlinmvvm.features.postlist.PostListAdapter
import com.elielmartinez.kotlinmvvm.model.Post
import me.eugeniomarletti.kotlin.metadata.shadow.utils.addToStdlib.cast

@BindingAdapter("visibleGone")
fun showHide(view: View, show: Boolean) {
    view.visibility = if (show) View.VISIBLE else View.GONE
}

@BindingAdapter("mutableText")
fun setMutableText(view: TextView, text: MutableLiveData<String>?) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null && text != null) {
        text.observe(parentActivity, Observer { value -> view.text = value })
    }
}

@BindingAdapter("data")
fun setData(view: RecyclerView, postList: LiveData<Resource<List<Post>>>) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null) {
        when (view.adapter) {
            is PostListAdapter -> {
                postList.observe(parentActivity, Observer { posts ->
                    posts.data?.let { view.adapter.cast<PostListAdapter>().updatePostList(it) }
                })
            }
        }

    }
}

@BindingAdapter("loadingVisible")
fun <T> showLoading(view: View, data: LiveData<Resource<T>>) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null) {
        data.observe(parentActivity, Observer {
            view.visibility = if (it.status == Status.LOADING) View.VISIBLE else View.GONE
        })
    }
}


@BindingAdapter("errorVisible")
fun <T> showError(view: View, data: LiveData<Resource<T>>) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null) {
        data.observe(parentActivity, Observer {
            view.visibility = if (it.status == Status.ERROR) View.VISIBLE else View.GONE
        })
    }
}

@BindingAdapter("errorMessage")
fun <T> showErrorMessage(view: TextView, data: LiveData<Resource<T>>) {
    val parentActivity: AppCompatActivity? = view.getParentActivity()
    if (parentActivity != null) {
        data.observe(parentActivity, Observer {
            if (it.status == Status.ERROR) {
                view.text = it.error?.localizedMessage
            }
        })
    }
}
