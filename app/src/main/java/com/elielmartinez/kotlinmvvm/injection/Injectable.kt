package com.elielmartinez.kotlinmvvm.injection

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable