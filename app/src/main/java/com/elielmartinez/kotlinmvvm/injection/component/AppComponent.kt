package com.elielmartinez.kotlinmvvm.injection.component

import android.app.Application
import com.elielmartinez.kotlinmvvm.MvvmApplication
import com.elielmartinez.kotlinmvvm.injection.module.AppModule
import com.elielmartinez.kotlinmvvm.injection.module.MainActivityModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        MainActivityModule::class,
        AppModule::class
    ]
)
interface AppComponent {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    fun inject(app: MvvmApplication)
}