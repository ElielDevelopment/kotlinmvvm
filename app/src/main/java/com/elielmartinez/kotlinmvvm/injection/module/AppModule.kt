package com.elielmartinez.kotlinmvvm.injection.module

import android.app.Application
import androidx.room.Room
import com.elielmartinez.kotlinmvvm.BuildConfig
import com.elielmartinez.kotlinmvvm.api.PostApi
import com.elielmartinez.kotlinmvvm.database.AppDatabase
import com.elielmartinez.kotlinmvvm.database.dao.PostDao
import dagger.Module
import dagger.Provides
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {
    /**
     * Provides the Post service implementation
     * @param retrofit The retrofit object used to instantiate the service
     * @return The Post Service implementation
     */
    @Provides
    @Singleton
    fun providePostApi(): PostApi {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_ENDPOINT)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .build()
            .create(PostApi::class.java)
    }

    @Singleton
    @Provides
    fun provideDb(app: Application): AppDatabase {
        return Room
            .databaseBuilder(app, AppDatabase::class.java, "posts.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun providePostDao(db: AppDatabase): PostDao {
        return db.postDao()
    }
}